﻿using System.Collections.Generic;
using System.Linq;

namespace CheckoutKata
{
    public class Checkout
    {
        private List<ISpecialOffer> _specialOffers;
        public List<Item> Items = new List<Item>();
        public Checkout(List<ISpecialOffer> offers) => _specialOffers = offers;

        public void ScanItem(string id) => Items.Add(ItemStore.Items.First(_ => _.Id.Equals(id)));
        public decimal Total() => Items.Sum(_ => _.Price) - SpecialOffers();
        private decimal SpecialOffers() => _specialOffers.Sum(_ => _.Discount(Items));
    }
}
