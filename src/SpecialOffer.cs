using System.Collections.Generic;
using System.Linq;
namespace CheckoutKata
{
    public interface ISpecialOffer 
    {
        decimal Discount(List<Item> items);
    }
    
    public class SpecialOffer : ISpecialOffer
    {
        public SpecialOffer(string id, int quantity, decimal offerPrice)
        {
            Id = id;
            Quantity = quantity;
            OfferPrice = offerPrice;
        }

        public string Id { get; set; }
        public int Quantity { get; set; }
        public decimal OfferPrice { get; set; }

        public decimal Discount(List<Item> items)
        {
            var matchOffer = items.Count(_ => _.Id.Equals(Id));
            return matchOffer >= Quantity ? (items.First(_ => _.Id.Equals(Id)).Price * Quantity) - OfferPrice : 0;
        }
    }
}