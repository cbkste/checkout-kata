using System.Collections.Generic;

namespace CheckoutKata
{
    public static class ItemStore
    {
        public static List<Item> Items = new List<Item>
        {
            new Item {
                Id = "A99",
                Price = 0.5M
            },
            new Item {
                Id = "B15",
                Price = 0.3M
            },
            new Item {
                Id = "C40",
                Price = 0.6M
            }
        };
    }
}
