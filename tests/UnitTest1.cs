using System.Collections.Generic;
using CheckoutKata;
using Moq;
using Xunit;

namespace CheckoutKataTests
{
    public class UnitTest1
    {
        private Mock<List<ISpecialOffer>> _offers = new Mock<List<ISpecialOffer>>();
        private Mock<ISpecialOffer> specialOffer = new Mock<ISpecialOffer>();
        private Mock<ISpecialOffer> specialOfferTwo = new Mock<ISpecialOffer>();

        [Fact]
        public void ScanItem_Should_Add_Item_To_ListOfScanned_Items()
        {
            //Arrange
            var checkout = new Checkout(_offers.Object);

            // Act
            checkout.ScanItem("A99");

            // Assert
            Assert.Single(checkout.Items);
        }

        [Fact]
        public void Total_Should_Return_Total_Price_Of_Items_Added()
        {
            //Arrange
            var checkout = new Checkout(_offers.Object);

            // Act
            checkout.ScanItem("A99");
            var result = checkout.Total();

            // Assert
            Assert.Equal(0.5M, result);
        }

        [Fact]
        public void Total_Should_Return_Total_Price_When_Multiple_Items_Added()
        {
            //Arrange
            var checkout = new Checkout(_offers.Object);
            // Act
            checkout.ScanItem("B15");
            checkout.ScanItem("C40");
            var result = checkout.Total();

            // Assert
            Assert.Equal(0.9M, result);
        }
        
        [Fact]
        public void Total_Should_Return_Correct_Price_When_B15_Has_Two_For_One_Special()
        {
            //Arrange
            specialOffer.Setup(_ => _.Discount(It.IsAny<List<Item>>())).Returns(0.15M);
            var checkout = new Checkout(new List<ISpecialOffer>{ specialOffer.Object });

            // Act
            checkout.ScanItem("B15");
            checkout.ScanItem("B15");
            var result = checkout.Total();

            // Assert
            Assert.Equal(0.45M, result);
        }

        [Fact]
        public void Total_Should_Return_Correct_Price_When_B15_Has_Two_For_One_Special_With_Additional_Item()
        {
            //Arrange
            specialOffer.Setup(_ => _.Discount(It.IsAny<List<Item>>())).Returns(0.15M);
            var checkout = new Checkout(new List<ISpecialOffer>{ specialOffer.Object });

            // Act
            checkout.ScanItem("B15");
            checkout.ScanItem("C40");
            checkout.ScanItem("B15");
            var result = checkout.Total();

            // Assert
            Assert.Equal(1.05M, result);
        }

        
        [Fact]
        public void Total_Should_Return_Correct_Price_When_B15_Has_Two_For_One_Special_With_Additional_Item_Two()
        {
            //Arrange
            specialOffer.Setup(_ => _.Discount(It.IsAny<List<Item>>())).Returns(0.15M);
            specialOfferTwo.Setup(_ => _.Discount(It.IsAny<List<Item>>())).Returns(0.20M);
            var checkout = new Checkout(new List<ISpecialOffer>{ specialOffer.Object, specialOfferTwo.Object });

            // Act
            checkout.ScanItem("C40");
            checkout.ScanItem("A99");
            checkout.ScanItem("B15");
            checkout.ScanItem("A99");
            checkout.ScanItem("B15");
            checkout.ScanItem("A99");
            var result = checkout.Total();

            // Assert
            Assert.Equal(2.35M, result);
        }
    }
}
