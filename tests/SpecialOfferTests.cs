using System.Collections.Generic;
using CheckoutKata;
using Xunit;

namespace CheckoutKataTests
{
    public class SpecialOfferTests
    {
        [Fact]
        public void Discount_Should_Correctly_Return_Discount_Amount__When_Item_Does_NotMatch_Offer()
        {
            //Arrange
            var checkout = new SpecialOffer("B99", 2, 0.45M);

            // Act
            var result = checkout.Discount(new List<Item>{ new Item { Id = "A99", Price = 0.50M } });

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void Discount_Should_Correctly_Return_Discount_Amount__When_Item_Matches_Offer()
        {
            //Arrange
            var checkout = new SpecialOffer("A99", 1, 0.45M);

            // Act
            var result = checkout.Discount(new List<Item>{ new Item { Id = "A99", Price = 0.50M } });

            // Assert
            Assert.Equal(0.05M, result);
        }
    }
}
